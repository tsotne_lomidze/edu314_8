<?php
/**
 * EDU314 Task8 (pwa slider)
 *
 * @category    EDU314
 * @package     Task8
 * @author      Tsotne Lomidze <tsotne.lomidze@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 */
declare(strict_types=1);

namespace EDU314\Task8\Model\Resolver;
use ScandiPWA\SliderGraphQl\Model\Resolver\Slider as SourceSlider;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\Resolver\ValueFactory;

use Scandiweb\Slider\Model\ResourceModel\Slider\CollectionFactory as SliderCollectionFactory;
use Scandiweb\Slider\Model\ResourceModel\Slide\CollectionFactory as SlideCollectionFactory;
use Scandiweb\Slider\Model\ResourceModel\Map\CollectionFactory as MapCollectionFactory;

/**
 * Class Slider
 * @package EDU314\Task8\Model\Resolver
 */
class Slider extends SourceSlider
{
    /**
     * @var ValueFactory
     */
    private $valueFactory;

    public function __construct(
        ValueFactory $valueFactory,
        SliderCollectionFactory $sliderCollectionFactory,
        SlideCollectionFactory $slideCollectionFactory,
        MapCollectionFactory $mapCollectionFactory
    ) {
        parent::__construct($valueFactory, $sliderCollectionFactory, $slideCollectionFactory, $mapCollectionFactory);
        $this->valueFactory = $valueFactory;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): Value {
        $result = function () {
            return null;
        };

        if (isset($args['id'])) {

            $slider = $this->sliderCollectionFactory->create();
            $slider->addFieldToFilter('slider_id', $args['id'])->load();
            $sliderData = $slider->getFirstItem()->getData();

            $slides = $this->slideCollectionFactory->create();
            $slides->addSliderFilter($args['id'])
                ->addStoreFilter()
                ->addDateFilter()
                ->addIsActiveFilter()
                ->addPositionOrder();

            $sliderData['slides'] = $slides->getData();

            $maps = $this->mapCollectionFactory->create();
            $maps = $maps->addSliderFilter($args['id'])
                ->addIsActiveFilter()
                ->getItems();

            foreach ($sliderData['slides'] as &$slide) {
                if (array_key_exists('image', $slide) && isset($slide['image'])){
                    $slide['image'] = DirectoryList::MEDIA . DIRECTORY_SEPARATOR . $slide['image'];
                }
                foreach ($maps as $map) {
                    if ($map['slide_id'] === $slide['slide_id']) {
                        $slide['maps'][] = $map;
                    }
                }
            }
            unset ($slide);

            $sliderData['date'] = date("l js \of F Y h:i:s");

            if ($sliderData) {
                $result = function () use ($sliderData) {
                    return $sliderData;
                };
            }
        }

        return $this->valueFactory->create($result);
    }

}
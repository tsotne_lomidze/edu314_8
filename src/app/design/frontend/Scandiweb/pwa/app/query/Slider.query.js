/**
 * @package EDU314/Task8
 */

import { Slider as SourceSlider } from 'SourceQuery/Slider.query';

/**
 * Slider Query
 * @class Slider
 */
export class Slider extends SourceSlider {
    _getSliderFields() {
        return [
            this._getSlidesField(),
            'slider_id',
            'date'
        ];
    }
}

export default new Slider();
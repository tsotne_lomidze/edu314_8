<?php
/**
 * @category EDU314
 * @package EDU314/Task8
 * @author Tsotne Lomidze <tsotne.lomidze@scandiweb.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'EDU314_Task8',
    __DIR__
);
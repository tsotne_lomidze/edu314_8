/**
 * @package EDU314/Task8
 */

import Image from 'Component/Image';
import Html from 'Component/Html';
import SourceSliderWidget from 'SourceComponent/SliderWidget/SliderWidget.component';
import './SliderWidget.extended.style';

/**
 * Homepage slider
 * @class SliderWidget
 */
export default class SliderWidget extends SourceSliderWidget {

    renderSlide = (slide, i) => {
        const {
            image,
            slide_text,
            isPlaceholder,
            title: block
        } = slide;
        const { slider } = this.props;

        return (
            <figure
                block="SliderWidget"
                elem="Figure"
                key={ i }
            >
                <div block="SliderWidget" elem="Date"><h2>{ slider.date || 'no date' }</h2></div>
                <Image
                    mix={ { block: 'SliderWidget', elem: 'FigureImage' } }
                    ratio="custom"
                    src={ image ? `/${image}` : '' }
                    isPlaceholder={ isPlaceholder }
                />
                <figcaption
                    block="SliderWidget"
                    elem="Figcaption"
                    mix={ { block } }
                >
                    <Html content={ slide_text || '' } />
                </figcaption>
            </figure>
        );

    };

}
